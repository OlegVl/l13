package ru.ovk.home.service;

import ru.ovk.home.entity.Project;
import ru.ovk.home.repository.ProjectRepository;

import java.util.List;

public class ProjectService {

    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Project create(String name) {
        if(name==null || name.isEmpty()) return null;
        return projectRepository.create(name);
    }

    public void clear() {
        projectRepository.clear();
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public Project findByIndex(int index) {
        if(index<0) return null;
        return projectRepository.findByIndex(index);
    }

    public Project findByName(String name) {
        if(name==null || name.isEmpty()) return null;
        return projectRepository.findByName(name);
    }

    public Project findById(Long id) {
        if(id==null) return null;
        return projectRepository.findById(id);
    }

    public Project removeByIndex(int id) {
        if(id<0) return null;
        return projectRepository.removeByIndex(id);
    }

    public Project removeById(Long id) {
        if(id==null) return null;
        return projectRepository.removeById(id);
    }

    public Project removeByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.removeByName(name);
    }

    public Project update(Long id, String name, String description) {
        return projectRepository.update(id, name, description);
    }
}
